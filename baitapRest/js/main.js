// function tính điểm trung bình với rest parameter
let getAverage = (...values) => {
  let average = null;
  values.forEach((valueInput) => {
    let score = document.getElementById(valueInput).value * 1;
    average += score;
  });
  return average / values.length;
};

let dtbKhoi1 = () => {
  let average = getAverage("inpToan", "inpLy", "inpHoa");
  document.querySelector("#tbKhoi1").innerHTML = average.toFixed(2);
};
let dtbKhoi2 = () => {
  let average = getAverage("inpVan", "inpSu", "inpDia", "inpEnglish");
  document.querySelector("#tbKhoi2").innerHTML = average.toFixed(2);
};
