const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderBtn = () => {
  for (let index = 0; index < colorList.length; index++) {
    let color = colorList[index];
    let btn = document.createElement("button");
    btn.style.backgroundColor = color;
    btn.className = "color-button" + " " + `${color}`;
    if (index == 0) {
      btn.className += " active";
    }
    btn.onclick = () => {
      document.querySelector("#house").className = "house";
      document.querySelector("#house").classList.add(`${color}`);
    };
    document.querySelector("#colorContainer").appendChild(btn);
  }
  // chạy class active
  let btnContainer = document.getElementById("colorContainer");
  let btns = btnContainer.getElementsByClassName("color-button");

  for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", () => {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      btns[i].className += " active";
    });
  }
};
renderBtn();
